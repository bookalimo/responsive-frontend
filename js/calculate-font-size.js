(function($) {
	var win = $(window),
		frameBox, nameBlock,
		fontSize = 14,
		coefficient = 2,
		delay = 200,
		breakpoint = 1024;

	function changeFontSize (size) {
		nameBlock.css({
			'font-size': size
		});
	}

	win.on({
		'load': function () {
			container = $('#main .container');
			frameBox = container.find('.frame-box');
			nameBlock = frameBox.find('.name');
		},
		'load resize orientationchange': function () {
			setTimeout(function () {
				var curWidth,
					sumPadding = parseInt(container.css('padding-left'), 10) * 2 + parseInt(frameBox.css('padding-left'), 10) * 2;
				fontSize = 14;

				if (win.width() < breakpoint) {
					curWidth = win.width() - sumPadding;
				} else {
					curWidth = frameBox.width() - sumPadding;
				}

				nameBlock.width(curWidth - coefficient);

				while (nameBlock[0].scrollWidth <= nameBlock[0].clientWidth) {
					changeFontSize(fontSize);
					fontSize++;
				}

				changeFontSize(fontSize - coefficient);
			}, delay);
		}
	});
})(jQuery);