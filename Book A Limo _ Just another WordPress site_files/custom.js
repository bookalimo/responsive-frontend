jQuery(function($){
	$('.info-box-title').matchHeight();

	$("input[type='text'],input[type='email'],textarea ").addClass("form-control");


	carosel_nav_center();

	menu_scroll_effect();

	popup_box();

        form_field_hiding();

        fancybox_call();
});


function fancybox_call(){
	var $ = jQuery;
	$(".fancybox").click(function(e){
		e.preventDefault();

		 var width = $("#header").width();

			if(width < 767){
				var src = $(this).attr("data-href");
				if(src == "undefined"){
					src = $(this).attr("href");
				}
			}else{
				var src = $(this).attr("href");
			}

			$.fancybox.open([{href:src}], {padding:4});

	});

	$('.open-popup').fancybox({
			openEffect: 'none',
			closeEffect: 'none'
		});

}
jQuery(window).load(function(){
	jQuery('.equal, .info-box-image').matchHeight();
	box();

});

jQuery(window).resize(function(){

	box();
	carosel_nav_center();

});

function box(){
	/* box */
	var $ = jQuery;
	$(".box").each(function(){
		var image_width = $("img",this).width();
		var image_height = $("img",this).height();

		$(".box-title",this).width(image_width);
		$(".box-title",this).height(image_height);

		$(".box-content",this).width(image_width);
		$(".box-content",this).height(image_height);

		var box_title_height = ($(".box-title h2",this).height())/2;
		//console.log(box_title_height);
		$(".box-title h2",this).css("margin-top","-"+(box_title_height)+"px");

		var box_content_height = ($(".box-content p",this).height())/2;
		//console.log(box_content_height);
		$(".box-content",this).animate({"margin-top":"-"+image_height+"px"});

		$(".box-content p",this).css("margin-top","-"+(box_content_height)+"px");
	});
}

function carosel_nav_center(){

	var $ = jQuery;
	$(".carousel-indicators li span").each(function(){
		var width = ($(this).width())/2;
		$(this).css("margin-left","-"+width+"px");
	});
}

function menu_scroll_effect(){
	var $ = jQuery;

	$('#header').scrollUpMenu({
	waitTime: 100,
	transitionTime: 150,
	menuCss: { 'position': 'fixed', 'top': '0'}
	});
}

function popup_box(){
	var $ = jQuery;

	var box_class = ".popup-container";

	var original_content = $(box_class).html();

	$("body").on("click",".popup",function(){


		var new_content = $(this).data("href");
		console.log($(new_content));
		if(($(new_content).length != 0)){
			$(box_class).html($(new_content).clone());
		}

	});

	$("body").on("click",".close-button",function(){
		//alert(original_content);
		$(box_class).html(original_content);

	});
}

function form_field_hiding(){
	var $ = jQuery;
	$("input[name='Own']").click(function() {
		var value = $(this).val();

	    if(value == "No"){
			$("#hiding-form").css("display","none");
		}else{
			$("#hiding-form").css("display","block");
		}
	});
}


jQuery(document).ready(function($) {
			$("#datepicker").bind("focus", function() {
	        $(this).data("kendoDatePicker").open();
	    });

			$("#timepicker").bind("focus", function() {
							$(this).data("kendoTimePicker").open();
			});
                	$("#datepicker").kendoDatePicker({
                		format: "yyyy-MM-dd",
										click: open,
                		  animation: {
                		   close: {

                		     duration: 300
                		   },
                		   open: {

                		     duration: 300

                		   }
                		  }
                		});


                		$("#timepicker").kendoTimePicker({
                			format: 'hh:mm tt',
                		    interval : 15,
                				  animation: {
                				   close: {

                				     duration: 300
                				   },
                				   open: {

                				     duration: 300
                				   }
                				  }
                		});
                });
