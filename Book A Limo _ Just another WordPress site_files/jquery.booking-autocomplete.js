(function (window, $, airportsSource) {

	var options = {
			maxItems: 5
		},
		activeClass = 'active',
		checkedClass = 'checked';

	function setData(params, isairport) {
		var resultData = {
			countrycode: params.country['short_name'],
			zipcode: params.postalCode['short_name'] || null,
			locality: params.locality['long_name'] || '',
			isairport: !!isairport
		};



		if (!isairport) {
			resultData.address = params.address;

			if (resultData.countrycode === 'US') {
				resultData.state = params.state['short_name'];
			}
		}
		else {
			var localAirports = [{"code":"ABE","name":"Allentown Lehigh Valley Intl"},{"code":"ABI","name":"ABILENE AIRPORT, TX"},{"code":"ABQ","name":"International"},{"code":"ABZ","name":"ABERDEENDA DYCE AIRPORT, Scotland"},{"code":"ACA","name":"Acapulco Airport, Mexico"},{"code":"ACY","name":"Atlantic City Intl"},{"code":"ADL","name":"Aledaide Airport, Australia"},{"code":"ADS","name":"Addison Airport"},{"code":"AEP","name":"BUENOS AIRES AIROPARCA AIRPORT (ARGENTINA)"},{"code":"AEX","name":"ALEXANDRIA AIRPORT (LA)"},{"code":"AFW","name":"Fort Worth Alliance AP"},{"code":"AGC","name":"Allegheny County"},{"code":"AGP","name":"MALAGA AIRPORT, Spain"},{"code":"AGR","name":"AGRA AIRPORT, India"},{"code":"AGS","name":"Bush Field"},{"code":"AIK","name":"AIKEN AIRPORT, SC"},{"code":"AJA"},{"code":"AKL","name":"AUCKLAND INT\u0027L AIRPORT, New Zeland"},{"code":"ALB","name":"Albany County"},{"code":"ALC","name":"ALICANTE-ELCH AIRPORT"},{"code":"ALO","name":"Waterloo Municipal"},{"code":"ALY","name":"ALEXANDRIA AIRPORT, Egypt"},{"code":"AMA","name":"International"},{"code":"AMD","name":"Ahmedabad Airport, India"},{"code":"AMS","name":"AMSTERDAM SCHIPHOL AIRPORT, Netherlands"},{"code":"ANC","name":"International"},{"code":"ANK","name":"ANKARA AIRPORT, Turkey"},{"code":"ANR","name":"ANTWERP DEURNE AIRPORT, Belgium"},{"code":"AOI","name":"ANCONA FALCONARA AIRPORT, Italy"},{"code":"AOO","name":"ALTOONA AIRPORT, PA"},{"code":"AP","name":"WROCLAW COPERNICUS"},{"code":"AP1","name":"AIRPORT PICK UP"},{"code":"APA","name":"CENTENNIAL AIRPORT"},{"code":"APC"},{"code":"APF","name":"Naples Municipal"},{"code":"ARA","name":"NEW IBERIA AIRPORT (LA)"},{"code":"ARB"},{"code":"ARN","name":"STOCKHOLM ARLANDA AIRPORT"},{"code":"ASE","name":"Aspen-Pitkin"},{"code":"ASH","name":"NASHUA AIRPORT (NH)"},{"code":"ATH","name":"ATHENS AIRPORT (GREECE)"},{"code":"ATL","name":"William B Hartsfield"},{"code":"ATQ","name":"Amritsar Airport (INDIA)"},{"code":"ATW","name":"Outagamie County"},{"code":"AUA","name":"QUEEN BEATRIX INTERNATIONAL AIRPORT"},{"code":"AUH","name":"Aurora Municipal A/P"},{"code":"AUS","name":"Robert Mueller Municipal"},{"code":"AVL","name":"Municipal"},{"code":"AVN"},{"code":"AVP","name":"Metropolitan Area"},{"code":"AVQ","name":"MANARA AIRPORT"},{"code":"AVX","name":"Catalina Island AP"},{"code":"AZA","name":"PHOENIX-MESA GATWAY AIRPORT"},{"code":"AZO","name":"Battle Creek International"},{"code":"BAQ","name":"BARRANQUILLA COLOMBIA AIRPORT"},{"code":"BCN","name":"BARCELONA AIRPORT, Spain"},{"code":"BCT","name":"Boca Raton Airport, FL"},{"code":"BDA","name":"BERMUDA AIRPORT"},{"code":"BDL","name":"Bradley International"},{"code":"BDS","name":"SALENTO AIRPORT"},{"code":"BED","name":"BEDFORD HANSCOM FIELD AIRPORT, MA"},{"code":"BFD","name":"BRADFORD AIRPORT, PA"},{"code":"BFI","name":"Boeing Field International"},{"code":"BFL","name":"Meadows Field"},{"code":"BFS","name":"BELFAST ALDERGROVE INT\u0027L AIRPORT (N.IRELAND)"},{"code":"BGI","name":"BARBADOS AIRPORT (BARBADOS)"},{"code":"BGM","name":"Binghamton Regional/Link Field"},{"code":"BGO","name":"BERGEN AIRPORT (NORWAY)"},{"code":"BGR","name":"International"},{"code":"BGY","name":"MILANO ORIO AL SAREO AIRPORT, Italy"},{"code":"BHM","name":"Birmingham Municipal"},{"code":"BHX","name":"BIRMINGHAM AIRPORT, United Kingdom"},{"code":"BIL","name":"Billings Logan International"},{"code":"BIO","name":"BILBAO AIRPORT, Spain"},{"code":"BIQ"},{"code":"BIS","name":"Bismarck"},{"code":"BJC","name":"Jeffco Airport (CO)"},{"code":"BJV","name":"BODRUM AIRPORT"},{"code":"BJX","name":"GUANAJUATO AIRPORT"},{"code":"BKA","name":"MOSCOW BYKOVO AIRPORT (RUSSIA)"},{"code":"BKG","name":"BRANSON AIRPORT"},{"code":"BKK","name":"BANGKOK AIRPORT (THAILAND)"},{"code":"BKW","name":"BECKLEY MEMORIAL AIRPORT (WV)"},{"code":"BLI","name":"BELLINGHAM AIRPORT (WA)"},{"code":"BLQ","name":"BOLOGNA PANICALE AIROIRT, Italy"},{"code":"BLR","name":"BANGLORE AIRPORT (INDIA)"},{"code":"BLV","name":"Belleville - Mid America AP"},{"code":"BMG"},{"code":"BMI","name":"Bloomington/Normal"},{"code":"BNA","name":"Metropolitan"},{"code":"BNE","name":"BRISBANE AIRPORT (AUSTRALIA)"},{"code":"BNJ","name":"BONN AIRPORT (GERMANY)"},{"code":"BOD","name":"BORDEAUX INT\u0027L AIRPORT (FRANCE)"},{"code":"BOG"},{"code":"BOH","name":"BOURNEMOUTH AIRPORT"},{"code":"BOI","name":"Air Term. (Gowen Field)"},{"code":"BOM","name":"MUBAI AIRPORT (INDIA)"},{"code":"BOS","name":"Logan International"},{"code":"BPT"},{"code":"BQK","name":"BRUNSWICK AIRPORT"},{"code":"BQN","name":"AGUADILLA AIRPORT (PUERTO RICO)"},{"code":"BRE","name":"BREMEN AIRPORT (GERMANY)"},{"code":"BRI","name":"BARI PALESE AIRPORT, Italy"},{"code":"BRL"},{"code":"BRN","name":"BERNE AIRPORT (SWITZERLAND)"},{"code":"BRS","name":"BRISTOL AIRPORT (UNITED KINGDOM)"},{"code":"BRU","name":"BRUSSELS ZAVENTEM AIRPORT (BELGIUM)"},{"code":"BSL","name":"BASEL AIRPORT (SWITZERLAND)"},{"code":"BTR","name":"Ryan"},{"code":"BTV","name":"International"},{"code":"BUD","name":"BUDAPEST AIRPORT"},{"code":"BUF","name":"Greater Buffalo International"},{"code":"BUR","name":"Burbank-Glendale-Pasadena"},{"code":"BVA","name":"PARIS BEAUVAIS AIRPORT (FRANCE)"},{"code":"BWG","name":"BOWLING GREEN AIRPORT (KY)"},{"code":"BWI","name":"Baltimore/Washington International"},{"code":"BZN","name":"BOZEMAN AIRPORT"},{"code":"CAE","name":"Columbia Metro"},{"code":"CAG","name":"CAGLIARI ELMAS AIRPORT, Italy"},{"code":"CAI","name":"CAIRO AIRPORT (EGYPT)"},{"code":"CAK","name":"Akron/Canton Regional"},{"code":"CAN","name":"GUANGZHOU BAIYUN INTERNATIONAL"},{"code":"CAX"},{"code":"CBG"},{"code":"CCS","name":"CARACAS AIRPORT (VENEZUELA)"},{"code":"CCU","name":"Kolkata Airport (INDIA)"},{"code":"CDG","name":"Charles de Gaulle Internationa"},{"code":"CEQ"},{"code":"CER"},{"code":"CFM","name":"CLERMONT FERRAND AIRPORT (FRANCE)"},{"code":"CFU","name":"CORFU "},{"code":"CGD","name":"CHENGDU AIRPORT (CHINA)"},{"code":"CGH"},{"code":"CGK","name":"JAKARTA AIRPORT"},{"code":"CGX","name":"Chicago Meigs Field"},{"code":"CHA","name":"Lovell Field"},{"code":"CHD","name":"MESA GATEWAY AIRPORT"},{"code":"CHO","name":"Albemarle"},{"code":"CHS","name":"AFB Municipal"},{"code":"CIA","name":"ROME CIAMPINO AIRPORT, Italy"},{"code":"CID","name":"Cedar Rapids Municipal"},{"code":"CKG","name":"CHONGQING AIRPORT (CHINA)"},{"code":"CLD","name":"Mc Clellan-Palomar, CARLSBAD CA"},{"code":"CLE","name":"Hopkins International"},{"code":"CLL","name":"Easterwood Airport"},{"code":"CLO","name":"ALFONSO BONILLA ARAGON "},{"code":"CLT","name":"Douglas"},{"code":"CMB"},{"code":"CMH","name":"Port Columbus International"},{"code":"CMI","name":"Willard University"},{"code":"CMN","name":"CASABLANCA AIRPORT (MOROCCO)"},{"code":"CNS","name":"CAIRNES AIRPORT (AUSTRALIA)"},{"code":"COR"},{"code":"COS","name":"Peterson Field"},{"code":"CPH","name":"COPENHAGEN AIRPORT (DENMARK)"},{"code":"CPR","name":"CASPER AIRPORT"},{"code":"CPT","name":"CAP TOWN AIRPORT (SOUTH AFRICA)"},{"code":"CQF"},{"code":"CRP","name":"International"},{"code":"CRW","name":"Yeager"},{"code":"CSG","name":"Columbus Metropolitan"},{"code":"CTA","name":"CATANIA AIRPORT, Italy"},{"code":"CTG","name":"CARTAGENA AIRPORT"},{"code":"CUN","name":"CANCUN AIRPORT (MEXICO)"},{"code":"CVG","name":"Cincinnati/No Kentucky"},{"code":"CWA","name":"Central Wisconsin"},{"code":"CWB","name":"CURITIBA AIRPORT"},{"code":"CYS","name":"CHEYENNE AIRPORT (WY)"},{"code":"DAB","name":"Regional"},{"code":"DAD","name":"DA NANG AIRPORT (VIETNAM)"},{"code":"DAL","name":"Love Field"},{"code":"DAM","name":"Damascus Airport (SYRIA)"},{"code":"DAY","name":"James Cox Dayton International"},{"code":"DBQ","name":"DUBUQUE REGIONAL AIRPORT"},{"code":"DBV","name":"DUBROVNIK INT\u0027L AIRPORT (CROATIA)"},{"code":"DCA","name":"National"},{"code":"DEC","name":"Decatur Airport"},{"code":"DEL","name":"DELHI INDIRA GANDHI INT\u0027L AIRPORT (INDIA)"},{"code":"DEN","name":"Denver International"},{"code":"DET","name":"Detroit City"},{"code":"DFW","name":"Dallas/FT Worth International"},{"code":"DHN","name":"Dothan Regional AP - DHN"},{"code":"DIJ","name":"DIJON-BOURGOGNE"},{"code":"DLH","name":"DULUTH AIRPORT (MN)"},{"code":"DME","name":"MOSCOW DOMODEDOVO AIRPORT (RUSSIA)"},{"code":"DMK","name":"BANGKOK AIRPORT"},{"code":"DNL","name":"DANIEL FIELD"},{"code":"DPA","name":"CHICAGO DUPAGE PRIVATE AIRPORT (IL)"},{"code":"DRO","name":"DURANGO AIRPORT (CO)"},{"code":"DRS","name":"DRESDEN AIRPORT (GERMANY)"},{"code":"DSM","name":"Des Moines International"},{"code":"DTW","name":"Wayne County"},{"code":"DUB","name":"DUBLIN AIRPORT (IRELAND)"},{"code":"DUD"},{"code":"DUJ","name":"Dubois - Jefferson County AP"},{"code":"DUS","name":"DUSSELDORF INT\u0027L AIRPORT (GERMANY)"},{"code":"DXB","name":"DUBAI AIRPORT (UAE)"},{"code":"DXR","name":"DANBURY AIRPORT (CT)"},{"code":"EAS"},{"code":"ECP","name":"PANAMA CITY AIRPORT"},{"code":"EDO","name":"Edinburgh Airport (SCOTLAND)"},{"code":"EFD","name":"Houston Ellington Field"},{"code":"EGE","name":"EAGLE COUNTY REGIONAL AIRPORT"},{"code":"EKA","name":"EUREKA MURRAY FIELD AIRPORT (CA)"},{"code":"ELM","name":"Regional Airport"},{"code":"ELP","name":"International"},{"code":"ERI","name":"International"},{"code":"ETH"},{"code":"EUG","name":"Mahlon Sweet Field"},{"code":"EVV","name":"Dress Regional"},{"code":"EVW","name":"Evanston Wyoming AP"},{"code":"EWR","name":"Newark Intl Airport"},{"code":"EXC","name":"EXECUTIVE AIPORT"},{"code":"EYW","name":"KEY WEST INTL AIRPORT"},{"code":"EZE","name":"DEEZEIZA INT\u0027L AIRPORT (ARGENTINA)"},{"code":"FAI","name":"International"},{"code":"FAO","name":"FARO AIRPORT (PORTUGAL)"},{"code":"FAR","name":"Hector Field"},{"code":"FAT","name":"Airterminal"},{"code":"FAY","name":"FAYETTEVILLE AIRPORT (NC)"},{"code":"FBO","name":"CENTENNIAL FBO"},{"code":"FCM","name":"Flying Cloud"},{"code":"FCO","name":"ROME FIUMICINO AIRPORT, Italy"},{"code":"FHB","name":"Fernandina Beach Municipal Airport AMELIA ISLAND"},{"code":"FLA","name":"FLAGSTAFF AIRPORT (AZ)"},{"code":"FLL","name":"International"},{"code":"FLO","name":"FLORENCE AIRPORT (SC)"},{"code":"FLR","name":"FLORENCE AIRPORT, Italy"},{"code":"FMO","name":"MUENSTER INT\u0027L AIRPORT (GERMANY)"},{"code":"FNT","name":"Bishop"},{"code":"FOD","name":"Fort Dodge Regional AP"},{"code":"FOG","name":"FOGGIA AIRPORT, Italy"},{"code":"FOK","name":"FRANCIS S GABRESKI AIRPORT WEST HAMPTON"},{"code":"FRA","name":"FRANKFURT GERMANY"},{"code":"FRG","name":"FARMINGDALE AIRPORT (NY)"},{"code":"FSD","name":"Regional(Jo Foss Field)"},{"code":"FSM","name":"Municipal"},{"code":"FTW","name":"FORT WAYNE AIRPORT (IN)"},{"code":"FTY","name":"Fulton County"},{"code":"FWA","name":"Municipal/Baer Field"},{"code":"FXE","name":"EXECUTIVE AIRPORT"},{"code":"FYV","name":"Municipal (Drake Field)"},{"code":"GAR","name":"GARRY AIRPORT (IN)"},{"code":"GDL","name":"GUADALAJARA AIRPORT"},{"code":"GEG","name":"International"},{"code":"GFK","name":"Grand Forks Intl AP"},{"code":"GIB","name":"GIBRALTAR AIRPORT (SPAIN)"},{"code":"GIG","name":"RIO DE JANEIRO INT\u0027L AIRPORT (BRAZIL)"},{"code":"GIR","name":"BARCELONA GIRONA AIRPORT (SPAIN)"},{"code":"GJT","name":"Walker Field"},{"code":"GLA","name":"GLASGOW AIRPORT (SCOTLAND)"},{"code":"GMP","name":"SEOUL GIMPO AIRPORT (SOUTH KOREA)"},{"code":"GNV","name":"J R Alison Municipal"},{"code":"GOA","name":"GENOA C.COLOMBUS AIRPORT, Italy"},{"code":"GOI","name":"GOA AIRPORT (INDIA)"},{"code":"GON","name":"NEW LONDON AIRPORT (CT)"},{"code":"GOT","name":"GOTHENBURG AIRPORT (SWEDEN)"},{"code":"GPT","name":"BILOXI GULFPORT AIRPORT"},{"code":"GRB","name":"Austin-Straubel Field"},{"code":"GRR","name":"Kent County International"},{"code":"GRU","name":"Guarulhos International Airpor SAO PULO,BRAZIL"},{"code":"GRX"},{"code":"GSO","name":"Piedmont Triad International"},{"code":"GSP","name":"Greenville-Spartanburg"},{"code":"GTF","name":"International"},{"code":"GUA","name":"Guatemala City Airport "},{"code":"GUC","name":"GUNNISON AIRPORT (CO)"},{"code":"GVA","name":"GENEVA COINTRIN AIRPORT (SWITZERLAND)"},{"code":"HAJ","name":"HANOVER AIRPORT (GERMANY)"},{"code":"HAM","name":"HAMBURG AIRPORT (GERMANY)"},{"code":"HDN","name":"Yampa Valley Regional AP"},{"code":"HEL","name":"HELSINKI AIRPORT (FINLAND)"},{"code":"HGH","name":"HGH HANGZHOU AIRPORT (CHINA)"},{"code":"HHH","name":"HILTON HEAD ISLAND AIRPORT (SC)"},{"code":"HII"},{"code":"HKG","name":"Hong Kong International Airpor"},{"code":"HKT","name":"PHUKET AIRPORT"},{"code":"HND","name":"TOKYO HANEDA DOMESTIC AIRPORT (JAPAN)"},{"code":"HNL","name":"International"},{"code":"HOU","name":"Hobby"},{"code":"HPN","name":"WHITE PLAINS AIRPORT"},{"code":"HRL","name":"Valley International"},{"code":"HSV","name":"Madison County"},{"code":"HUF","name":"Hulman Field"},{"code":"HVN","name":"New Haven - Tweed New Haven Ai"},{"code":"HYD","name":"HYDERABAD AIRPORT (INDIA)"},{"code":"IAD","name":"Dulles International"},{"code":"IAG","name":"Niagara Falls Intl AP"},{"code":"IAH","name":"Intercontinental"},{"code":"ICN","name":"SEOUL INCHEON AIRPORT (SOUTH KOREA)"},{"code":"ICT","name":"Metropolitan Area"},{"code":"IDA","name":"IDAHO FALLS AIRPORT (ID)"},{"code":"IFP","name":"Laughlin Bullhead City Intl. A"},{"code":"IGU","name":"FOZ DO IGUASSU "},{"code":"IGU1","name":"FOZ DO IGUASS"},{"code":"IKK","name":"Greater Kankakee Airport"},{"code":"ILG","name":"Wilmington - New Castle Airpor"},{"code":"ILM","name":"New Hanover County"},{"code":"ILN","name":"Clinton Field"},{"code":"IND","name":"International"},{"code":"INN"},{"code":"INV","name":"INVERNESS AIRPORT (SCOTLAND)"},{"code":"IPT","name":"WILLIAMSPORT AIRPORT (PA)"},{"code":"ISB"},{"code":"ISM","name":"KISSIMMEE MUNICIPAL AIRPORT"},{"code":"ISP","name":"Long Island/Mac Arthur"},{"code":"IST","name":"ISTANBUL AIRPORT (TURKEY)"},{"code":"ITH","name":"ITHACA AIRPORT (NY)"},{"code":"ITM","name":"Osaka-Itami International Airport"},{"code":"ITO","name":"Hilo Intl Airport"},{"code":"IWA","name":"Mesa - Williams Gateway Airpor"},{"code":"IXC","name":"Chandigarth Airport (INDIA)"},{"code":"IXU","name":"AURANGABAD AIRPORT (INDIA)"},{"code":"IZM","name":"IZMIR AIRPORT (TURKEY)"},{"code":"JAC","name":"Jackson Hole Airport (WY)"},{"code":"JAN","name":"Jackson International"},{"code":"JAX","name":"International"},{"code":"JFK","name":"John F Kennedy International"},{"code":"JHW","name":"JAMESTOWN AIRPORT (NY)"},{"code":"JMK"},{"code":"JNB"},{"code":"JNU","name":"JUNEAU AIRPORT (AK)"},{"code":"JRS"},{"code":"JST","name":"Johnstown - Cambria County Air"},{"code":"JTR","name":"SANTORINI AIRPORT (GREECE)"},{"code":"KBP","name":"KIEV BORYSPIL AIRPORT"},{"code":"KEF","name":"REYKJAVIK INT\u0027L AIRPORT (ICELAND)"},{"code":"KIX","name":"Kansai International Airport"},{"code":"KMG","name":"KUNMING AIRPORT (CHINA)"},{"code":"KOA","name":"Kona Intl Airport - H"},{"code":"KRK","name":"Krakow Airport (POLAND)"},{"code":"KTM"},{"code":"KUL","name":"KUALA LUMPUR INT\u0027L AIRPORT (MALAYSIA)"},{"code":"KWI","name":"KUWAIT CITY AIRPORT"},{"code":"KWL","name":"GUILIN AIRPORT (CHINA)"},{"code":"KYW","name":"KEY WEST AIRPORT (FL)"},{"code":"LAN","name":"Capital City Airport"},{"code":"LAS","name":"McCarren International Airport"},{"code":"LAX","name":"Los Angeles Int\u0027l Airport"},{"code":"LBA","name":"LEEDS AIRPORT (UNITED KINGDOM)"},{"code":"LBB","name":"International"},{"code":"LBE","name":"Westmoreland Co. - Arnold Palm"},{"code":"LBG","name":"PARIS LE BOURGET AIRPORT (FRANCE)"},{"code":"LCK","name":"Rickenbacker Intl AP"},{"code":"LCY","name":"LONDON CITY AIRPORT (UNITED KINGDOM)"},{"code":"LDE","name":"LOURDES TARBES AIRPORT"},{"code":"LEB","name":"Lebanon Municipal Airport"},{"code":"LED","name":"ST.PETERSBURG AIRPORT (RUSSIA)"},{"code":"LEH"},{"code":"LEJ","name":"Leipzig Airport (GERMANY)"},{"code":"LEN","name":"LEON AIRPORT"},{"code":"LEN1","name":"LEON AIRPORT (SPAIN)"},{"code":"LEX","name":"Blue Grass"},{"code":"LFT","name":"Lafayette Regional"},{"code":"LGA","name":"LaGuardia"},{"code":"LGB","name":"Long Beach Municipal"},{"code":"LGW","name":"Gatwick LONDON"},{"code":"LHR","name":"HEATHROW LONDON"},{"code":"LIH","name":"Lihue Airport - Kauai"},{"code":"LIM","name":"LIMA AIRPORT (PERU)"},{"code":"LIN","name":"MILANO LINATE AIRPORT, Italy"},{"code":"LIR","name":"LIBERIA COSTA RICA AIRPORT"},{"code":"LIS","name":"LISBON AIRPORT (PORTUGAL)"},{"code":"LIT","name":"Regional Airport"},{"code":"LIV","name":"LIVERPOOL INT\u0027L AIRPORT (UNITED KINGDOM)"},{"code":"LMT","name":"Klamath Falls Intl AP"},{"code":"LNA","name":"LANCASTER AIRPORT (PA)"},{"code":"LNK","name":"LINCOLN AIRPORT (NE)"},{"code":"LNS","name":"Lancaster Airport - LNS"},{"code":"LNY","name":"Lanai Airport - Lanai - LNY"},{"code":"LRM","name":"LA ROMANA DOMINICAN REPUBLIC"},{"code":"LRU","name":"Las Cruces Intl AP"},{"code":"LSE","name":"LA CROSSE AIRPORT"},{"code":"LTN","name":"LUTON AIRPORT (UNITED KINGDOM)"},{"code":"LUX","name":"LUXEMBOURG AIRPORT (LUXEMBOURG)"},{"code":"LWB","name":"GREENBRIAR VALLEY AIRPORT (WV)"},{"code":"LWS","name":"Lewinston-Nez Perce County Reg"},{"code":"LYN","name":"LYON AIRPORT (FRANCE)"},{"code":"LYS","name":"LYON AIRPORT (FRANCE)"},{"code":"MAA","name":"CHENNAI AIRPORT (INDIA)"},{"code":"MAD","name":"MADRID BARAJAS AIPRORT (SPAIN)"},{"code":"MAF","name":"Midland odessa Airport"},{"code":"MAN","name":"MANCHESTER RINGWAT AIRPORT (UNITED KINGDOM)"},{"code":"MBJ","name":"MONTEGO AIRPORT (JAMAICA)"},{"code":"MBS","name":"Saginaw Bay City Midland - MBS"},{"code":"MCG","name":"KNOXVILLE McGHEE TYSON AIRPORT (TN)"},{"code":"MCI","name":"International"},{"code":"MCM","name":"MONTE CARLO AIRPORT (FRENCH RIVIERA)"},{"code":"MCN"},{"code":"MCO","name":"Orlando Intl Airport"},{"code":"MDE","name":"ENRIQUE OLAYA HERRERA"},{"code":"MDI","name":"MERIDA AIRPORT"},{"code":"MDT","name":"Harrisburg International"},{"code":"MDW","name":"CHICAGO MIDWAY"},{"code":"MEL","name":"MELBOURNE AIRPORT (AUSTRALIA)"},{"code":"MEM","name":"International"},{"code":"MEX","name":"MEXICO CITY INTERNATIONAL AIRP"},{"code":"MFD","name":"Mansfield Lahm Airport"},{"code":"MFE","name":"McAllen - Miller Intl"},{"code":"MGA","name":"Managua International Airport"},{"code":"MGM","name":"MONTGOMERY AIRPORT (AL)"},{"code":"MHR","name":"Sacramento Mather Airport"},{"code":"MHT","name":"Municipal"},{"code":"MIA","name":"International"},{"code":"MID","name":"MIDLAND JACK BARSTOW AIRPORT (MI)"},{"code":"MIS","name":"MISSOULA AIRPORT (MT)"},{"code":"MKC","name":"Downtown"},{"code":"MKE","name":"General Mitchell"},{"code":"MKG","name":"Muskegon County Airport"},{"code":"MKK","name":"Kaunakakai Airport - Molokai"},{"code":"MLA","name":"VALLETTA AIRPORT (MALTA)"},{"code":"MLB","name":"Melbourne International Airpor"},{"code":"MLI","name":"Quad-City"},{"code":"MMU","name":"Morristown Municipal Airport"},{"code":"MNL","name":"MANILA INTERNATIONAL "},{"code":"MNZ","name":"MANASSAS PRIVATE AIRPORT (VIRGINIA)"},{"code":"MOB","name":"Mobile Regional Airport"},{"code":"MOD","name":"MODESTO AIRPORT (CA)"},{"code":"MRI","name":"Anchorage Merrill Field Airpor"},{"code":"MRS","name":"MARSEILLE AIRPORT (FRANCE)"},{"code":"MRY","name":"Monterey Peninsula"},{"code":"MSL","name":"Muscle Shoals - Northwest Alab"},{"code":"MSN","name":"Truax Field"},{"code":"MSO","name":"Johnson-Bell Field"},{"code":"MSP","name":"Minneapolis-St. Paul Intl. Air"},{"code":"MST","name":"MAASTRICHT AIRPORT (NETHERLANDS)"},{"code":"MSY","name":"International"},{"code":"MTJ","name":"Montrose Regional Airport"},{"code":"MTY","name":"MONTERREY MEXICO AIRPORT"},{"code":"MUC","name":"MUNICH FRANZ JOSEF STRAUSS AIRPORT (GERMANY)"},{"code":"MVD","name":"MONTEVIDEO AIRPORT (URUGUAY)"},{"code":"MXP","name":"MILANO MALPENSA AIRPORT, Italy"},{"code":"MYR","name":"MYRTLE BEACH AIRPORT (SC)"},{"code":"NAA","name":"AP NOT ON SYSTEM"},{"code":"NAB","name":"ALBANY AIRPORT (GA)"},{"code":"NAP","name":"NAPLES CAPODICHINO AIRPORT, Italy"},{"code":"NAR","name":"TOKYO NARITA INT\u0027L AIRPORT (JAPAN)"},{"code":"NAS","name":"NASSAU AIRPORT"},{"code":"NCE","name":"NICE AIRPORT (FRENCH RIVIERA)"},{"code":"NCL","name":"NEWCASTLE AIRPORT (UNITED KINGDOM)"},{"code":"NEW","name":"NEW ORLEANS LAKE SHORE AIRPORT (LA)"},{"code":"NKG","name":"NANJING AIRPORT (CHINA)"},{"code":"NKM","name":"NAGOYA INTERNATIONAL"},{"code":"NOI","name":"HANOI NOI BAI AIRPORT (VIETNAM)"},{"code":"NTE","name":"NANTES AIRPORT (FRANCE)"},{"code":"NUE","name":"NUREMBERG AIRPORT (GERMANY)"},{"code":"NZJ","name":"El Toro Airport"},{"code":"OAK","name":"Metropolitan Oak International"},{"code":"OCE","name":"OCEAN CITY SALSBURY AIRPORT (MD)"},{"code":"ODS","name":"ODESSA AIRPORT"},{"code":"OGG","name":"Kahului"},{"code":"OJU","name":"LJUBLJANA AIRPORT (SLOVENIA)"},{"code":"OKC","name":"Will Rogers World"},{"code":"OLB","name":"Olbia Airport, Italy"},{"code":"OMA","name":"Eppley Airfield"},{"code":"ONT","name":"Metropolitan Area"},{"code":"OOL","name":"GOLD COAST AIRPORT "},{"code":"OPF","name":"OPA-LOCKA"},{"code":"OPO","name":"PORTO AIRPORT (PORTUGAL)"},{"code":"ORD","name":"O\u0027Hare International"},{"code":"ORF","name":"International"},{"code":"ORK","name":"CORK AIRPORT (IRELAND)"},{"code":"ORL","name":"ORLANDO EXECUTIVE AIRPORT (FL)"},{"code":"ORY","name":"PARIS ORLY AIRPORT (FRANCE)"},{"code":"OSL","name":"OSLO AIRPORT (NORWAY)"},{"code":"OST"},{"code":"OSU","name":"OHIO STATE UNIVERSITY AIRPORT"},{"code":"OTP","name":"BUCHAREST AIRPORT"},{"code":"OVD","name":"ASTURIAS AIRPORT (SPAIN)"},{"code":"OXF","name":"OXFORD AIRPORT (UNITED KINGDOM)"},{"code":"PAC","name":"ALBROOK MARCOS A GELABERT AIRPORT"},{"code":"PAU"},{"code":"PBC","name":"PUBLA AIRPORT MEXICO"},{"code":"PBI","name":"International"},{"code":"PCT","name":"PRINCETON AIRPORT (NJ)"},{"code":"PDK","name":"De Kalb/Peachtree"},{"code":"PDX","name":"International"},{"code":"PEG","name":"Perugia Airport, Italy"},{"code":"PEK","name":"BEIJING AIRPORT (CHINA)"},{"code":"PER","name":"Perth International (AUSTRALIA)"},{"code":"PFN","name":"Bay County"},{"code":"PGA","name":"Page Municipal Airport,AZ -PGA"},{"code":"PGD","name":"PUNTA GORDA AIRPORT"},{"code":"PGO","name":"Pagosa Springs - Archuleta Cou"},{"code":"PHF","name":"Newport News-Williamsburg Intl"},{"code":"PHL","name":"PHILADELPHIA INTERNATIONAL"},{"code":"PHU","name":"HUE BAI AIRPORT (VIETNAM)"},{"code":"PHX","name":"Sky Harbor International"},{"code":"PIA","name":"Greater Peoria"},{"code":"PIE","name":"St. Petersburg-Clearwater Intl"},{"code":"PIR","name":"PIERRE AIRPORT (SD)"},{"code":"PIT","name":"Metropolitan Area"},{"code":"PLS","name":"TURK\u0026CAICOS ISLAND AIRPORT"},{"code":"PMD","name":"Palmdale Airport - PMD"},{"code":"PMF","name":"PARMA AIRPORT, Italy"},{"code":"PMI","name":"PALMA DE MALLORCA AIRPORT (SPAIN)"},{"code":"PMO","name":"PALERMO PUNTA RAISI AIRPORT, Italy"},{"code":"PNA","name":"PAMPLONA AIRPORT"},{"code":"PNA1","name":"PAMPLONA AIRPORT"},{"code":"PNS","name":"Regional"},{"code":"POP","name":"Puerto Plata La Union airport"},{"code":"PQI","name":"Presque Isle - Northern Maine"},{"code":"PRA","name":"PRAGUE AIRPORT (CZECH REP)"},{"code":"PRC","name":"Prescott Municipal Airport - P"},{"code":"PRG","name":"PRAGUE AIRPORT (CZECH REP)"},{"code":"PSA","name":"PISA AIRPORT, Italy"},{"code":"PSM","name":"Portsmouth - Pease Intl. Trade"},{"code":"PSP","name":"Metropolitan Area"},{"code":"PSR","name":"LIBERI AIRPORT, Italy"},{"code":"PTY"},{"code":"PUJ","name":"PUNTA CANA AIRPORT"},{"code":"PUL","name":"PULA AIRPORT (CROATIA)"},{"code":"PVD","name":"Theodore Francis"},{"code":"PVG","name":"SHANGHAI PUDONG AIRPORT (CHINA)"},{"code":"PVR","name":"PUERTO VALLARTA "},{"code":"PVU","name":"PROVO AIRPORT (UT)"},{"code":"PWK","name":"Pal-Waukee"},{"code":"PWM","name":"International Jetport"},{"code":"QUI","name":"QUITO AIRPORT (ECUADOR)"},{"code":"RAP","name":"Rapid City Regional Airport -"},{"code":"RDG","name":"Reading Regional Airport - RDG"},{"code":"RDU","name":"Raleigh/Durham"},{"code":"REG","name":"REGGIO DI CALABRIA AIRPORT, Italy"},{"code":"RFD","name":"Greater Rockford"},{"code":"RIC","name":"International (Byrd Field)"},{"code":"RIJ","name":"RIJEKA AIRPORT (CROATIA)"},{"code":"RIL","name":"Rifle - Garfield County Region"},{"code":"RKP","name":"Rockport - Arkansas County Air"},{"code":"RKS","name":"Rock Springs - Sweetwater Coun"},{"code":"RNO","name":"Cannon International"},{"code":"ROA","name":"Municipal"},{"code":"ROC","name":"Monroe County"},{"code":"ROT","name":"ROTTERDAM AIRPORT (NETHERLANDS)"},{"code":"RST","name":"ROCHESTER AIRPORT (MN)"},{"code":"RSW","name":"Southwest Florida Regional"},{"code":"RUT","name":"Rutland State Airport"},{"code":"SAC","name":"Sacramento Executive Airport -"},{"code":"SAF","name":"SANTA FE AIRPORT (NM)"},{"code":"SAN","name":"Lindberg Field"},{"code":"SAO","name":"SAO PAOLO AIRPORT (BRAZIL)"},{"code":"SAT","name":"San Antonio International Airport"},{"code":"SAV","name":"Savannah International Airport"},{"code":"SAW","name":"SABIH GOKCEN INTERNATIONAL AIRPORT"},{"code":"SAY"},{"code":"SBA","name":"Municipal"},{"code":"SBN","name":"St Joseph Co"},{"code":"SBO","name":"Santa Barbara Municipal Airpor"},{"code":"SBP","name":"SAN LUIS OBISPO AIRPORT (CA)"},{"code":"SBY","name":"SALISBURY"},{"code":"SCF","name":"PHOENIX SCOTTSDALE AIRPORT (AZ)"},{"code":"SCG","name":"Scottsdale Airport - SCG"},{"code":"SCL","name":"SANTIAGO AIRPORT (CHILE)"},{"code":"SCQ","name":"SANTIAGO DE COMPUSTELA"},{"code":"SDF","name":"Standiford Field"},{"code":"SDM","name":"BROWN FIELD MUNICIPAL AIRPORT "},{"code":"SDQ","name":"SANTO DOMINGO AIRPORT"},{"code":"SDU","name":"RIO DE JANEIRO DOMESTIC AIRPORT (BRAZIL)"},{"code":"SEA","name":"Seattle/Tacoma International"},{"code":"SFB","name":"Orlando Sanford Airport - SFB"},{"code":"SFO","name":"International"},{"code":"SGF","name":"Regional Airport"},{"code":"SGN","name":"HO CHI MINH AIRPORT (VIETNAM)"},{"code":"SGU","name":"SAINT GEORGE AIRPORT (UT)"},{"code":"SHA","name":"SHANGHAI SHA HONG QIAO AIRPORT (CHINA)"},{"code":"SHJ","name":"SHARJAH AIRPORT (UAE)"},{"code":"SHV","name":"Regional"},{"code":"SIN","name":"Singapore International Changi"},{"code":"SIP","name":"SIMFEROPOL AIRPORT"},{"code":"SJC","name":"Municipal"},{"code":"SJD","name":"LOS CABOS INTERNATIONAL AIRPORT"},{"code":"SJJ","name":"SARAJEVO AIRPORT"},{"code":"SJO","name":"SAN JOSE AIRPORT (COSTA RICA)"},{"code":"SJU","name":"San Juan Luis MuÃ±oz Marin "},{"code":"SLC","name":"International"},{"code":"SLK","name":"SARANAC AIRPORT (NY)"},{"code":"SMF","name":"Metropolitan"},{"code":"SMO","name":"Santa Monica Municipal Airport"},{"code":"SMX","name":"SANTA MARIA AIRPORT (CA)"},{"code":"SNA","name":"John Wayne International Airport"},{"code":"SNN","name":"SHANNON AIRPORT (IRELAND)"},{"code":"SOF","name":"SOFIA AIRPORT (BULGARIA)"},{"code":"SOU","name":"SOUTHAMPTON AIRPORT (UNITED KINGDOM)"},{"code":"SPI","name":"Capital"},{"code":"SPL","name":"SPLIT AIRPORT (CROATIA)"},{"code":"SRQ","name":"Bradenton"},{"code":"STL","name":"Lambert-St Louis International"},{"code":"STN","name":"STANSTED AIRPORT (UNITED KINGDOM)"},{"code":"STO","name":"STOCKHOLM INT\u0027L AIRPORT (SWEDEN)"},{"code":"STR","name":"STRASBOURG AIRPORT (FRANCE)"},{"code":"STS","name":"SANTA ROSA AIRPORT (CA)"},{"code":"STT","name":"ST THOMAS AIRPORT"},{"code":"STU","name":"STUTTGART AIRPORT (GERMANY)"},{"code":"SUF"},{"code":"SUN","name":"SUN VALLEY AIRPORT"},{"code":"SUS","name":"Spirit Of St Louis"},{"code":"SUX","name":"Sioux Gateway"},{"code":"SVO","name":"SHEREMETEVO"},{"code":"SVQ","name":"SEVILLA AIRPORT (SPAIN)"},{"code":"SWF","name":"Stewart"},{"code":"SXF","name":"BERLING SCHOENESELD AIRPORT (GERMANY)"},{"code":"SXM","name":"ST MAARTIN"},{"code":"SYD","name":"SYDNEY AIRPORT (AUSTRALIA)"},{"code":"SYR","name":"Hancock International"},{"code":"SZG","name":"SALZBURG AIRPORT (AUSTRIA)"},{"code":"SZV"},{"code":"SZX","name":"SHENZHEN AIRPORT"},{"code":"TAO","name":"TAORMINA AIRPORT, Italy"},{"code":"TAR","name":"LOURDES TARBES AIRPORT (FRANCE)"},{"code":"TBD","name":"TBD"},{"code":"TBI","name":"TBILISI AIRPORT"},{"code":"TCL","name":"Tuscaloosa Municipal Airport -"},{"code":"TEB","name":"TETERBORO AIRPORT (NJ)"},{"code":"TEX","name":"Telluride Regional Airport - T"},{"code":"THE"},{"code":"TIA","name":"TIRANA ALBANIA"},{"code":"TIX","name":"Titusville - Cocoa Airport Aut"},{"code":"TLH","name":"Municipal"},{"code":"TLS","name":"TOULOUSE AIRPORT (FRANCE)"},{"code":"TLV","name":"TEL AVIV BEN GURION AIRPORT (ISRAEL)"},{"code":"TOL","name":"Express"},{"code":"TOR","name":"MADRID TORREJON AIRPORT (SPAIN)"},{"code":"TOS","name":"TROMSO AIRPORT"},{"code":"TOU","name":"TOULON AIRPORT (FRANCE)"},{"code":"TPA","name":"International"},{"code":"TPE","name":"TAIPEI AIRPORT (TAIWAN)"},{"code":"TRA","name":"TRAVERSE CITY AIRPORT (MI)"},{"code":"TRD","name":"TRONDHEIM NORWAY AIRPORT"},{"code":"TRE","name":"TREVISO AIRPORT, Italy"},{"code":"TRI","name":"Tri-Cities Regional Airport -"},{"code":"TRN","name":"TORINO CASELLE AIRPORT, Italy"},{"code":"TRS","name":"TRIESTE AIRPORT, Italy"},{"code":"TSN","name":"TIANJIN AIRPORT"},{"code":"TTN","name":"TRENTON AIRPORT (NJ)"},{"code":"TUL","name":"International"},{"code":"TUP","name":"TUPELO AIRPORT (MS)"},{"code":"TUS","name":"International"},{"code":"TVC","name":"Traverse City - Cherry Capital"},{"code":"TXK","name":"Texarkana Regional Airport"},{"code":"TXL","name":"BERLIN TEQEL AIRPORT (GERMANY)"},{"code":"TYS","name":"Mc Ghee Tyson"},{"code":"UKY","name":"Kyoto Airport"},{"code":"UNV","name":"University Park Airport"},{"code":"UX","name":"AIR EUROPA"},{"code":"VAL","name":"VALENCIA AIRPORT (SPAIN)"},{"code":"VCE","name":"VCE(MARCO POLO)"},{"code":"VCT","name":"Victoria Regional Airport"},{"code":"VIE","name":"VIENNA AIRPORT (AUSTRIA)"},{"code":"VKO","name":"MOSCOW VNUKOVO AIRPORT (RUSSIA)"},{"code":"VLD","name":"Valdosta Regional Airport"},{"code":"VNY","name":"Van Nuys Regional Airport"},{"code":"VPS","name":"FORT WALTON AIRPORT (FL)"},{"code":"VPZ","name":"Valparaiso - Porter County Mun"},{"code":"VRB"},{"code":"VRN","name":"VERONA AIRPORT, Italy"},{"code":"VUO","name":"VANCOUVER WA"},{"code":"WAC","name":"WACO AIRPORT (TX)"},{"code":"WAW","name":"WARSAW AIRPORT (POLAND)"},{"code":"WEL","name":"WELLINGTON AIRPORT (NEW ZEALAND)"},{"code":"WES","name":"WESTPORT AIRPORT (MA)"},{"code":"WOL","name":"WOLKES AIRPORT (PA)"},{"code":"WRI"},{"code":"WUH","name":"WUHAN AIRPORT (CHINA)"},{"code":"WVI","name":"Watsonville Municipal Airport"},{"code":"XIY","name":"XIAN INTERNATIONAL AIRPORT (CHINA)"},{"code":"XNA","name":"Springdale - Northwest Arkansa"},{"code":"XZI","name":"NANCY/METZ ITALY AIRPORT"},{"code":"YEG","name":"EDMONTON AIRPORT (CANADA)"},{"code":"YGK","name":"KINGSTON AIRPORT (CANADA)"},{"code":"YHZ","name":"HALIFAX AIRPORT (CANADA)"},{"code":"YIP","name":"Willow Run"},{"code":"YMM","name":"Fort McMurray Airport (CANADA)"},{"code":"YMX","name":"MONTREAL MIRABEL AIRPORT (CANADA)"},{"code":"YOW","name":"Ottawa International Airport"},{"code":"YPR","name":"PRINCE RUBERT"},{"code":"YQB","name":"QUEBEC AIRPORT (CANADA)"},{"code":"YQR","name":"REGINA AIRPORT (CANADA)"},{"code":"YTZ","name":"TORONTO ISLAND AIRPORT (CANADA)"},{"code":"YUL","name":"Montreal Dorval Canada"},{"code":"YVR","name":"Vancouver Internation Airport"},{"code":"YWG","name":"JAMES ARMSTRONG RICHARDSON INT AIRPORT"},{"code":"YXE","name":"SASKATOON INT AIRPORT"},{"code":"YXT","name":"TERRACE AIRPORT "},{"code":"YXU","name":"LONDON AIRPORT (ZZCANADA)"},{"code":"YYC","name":"Calgary International Airport"},{"code":"YYJ","name":"VICTORIA AIRPORT "},{"code":"YYZ","name":"Toronto Canada - Lester Pearson"},{"code":"ZAD","name":"ZADAR AIRPORT (CROATIA)"},{"code":"ZAG","name":"ZAGREB AIRPORT (CROATIA)"},{"code":"ZQN","name":"QUEENSTOWN AIRPORT"},{"code":"ZRH","name":"ZURICH AIRPORT (SWITZERLAND)"}]
			resultData.airportcode = params.code;
			//resultData.airportcode = params.code;
			//resultData.airlinecode = 'UA';
			//resultData.flightno = '123';
		}

		return resultData;
	}

	function sendGeoRequest(requestParams, callback) {
		var country = {}, state = {}, locality = {}, postalCode = {},
			geocoder = new google.maps.Geocoder();

		geocoder.geocode({
			address: requestParams.label
		}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				$.each(results, function () {
					$.each(this['address_components'], function () {
						if (this.types.indexOf('country') !== -1) {
							country = this;
						}

						if (this.types.indexOf('administrative_area_level_1') !== -1) {
							state = this;
						}

						if (this.types.indexOf('locality') !== -1) {
							locality = this;
						}

						if (this.types.indexOf('postal_code') !== -1) {
							postalCode = this;
						}
					});
				});

				if (typeof callback === 'function') {
					callback(setData({
						address: requestParams.label,
						code: requestParams.code,
						country: country,
						state: state,
						locality: locality,
						postalCode: postalCode
					}, requestParams.isairport));
				}
			}
		});
	}

	airportsSource = $.map(airportsSource || [], function (item) {
		return {
			code: item.code,
			label: item.code + ' - ' +item.name,
			isairport: true
		};
	});

	$(function () {
		var autocompleteService,
			bookingForm = $('#booking-form form'),
			body = $('body'),
			loaderClass = 'loader',
			submitBtn = bookingForm.find('[data-role="submit"]'),
			bookBtn = bookingForm.find('[data-role="book"]')
			datePickerInput = $('[data-role="datepicker"]'),
			timePickerInput = $('[data-role="timepicker"]'),
			pickupInput = $('[data-role="booking-autocomplete-pickup"]'),
			dropoffInput = $('[data-role="booking-autocomplete-dropoff"]'),
			allBookingInputs = datePickerInput.add(timePickerInput).add(pickupInput).add(dropoffInput);

		allBookingInputs.each(function () {
			$(this).val('');
		});

		if (typeof google !== 'undefined') {
			autocompleteService = new google.maps.places.AutocompleteService();
		}

		bookingForm.formValidator({
			onSubmit: function (event, ui) {
				var pickupData, dropoffData;
				event.preventDefault();
				if (ui.passValid && pickupInput.data('item-info') && dropoffInput.data('item-info')) {
					pickupData = pickupInput.data('item-info'),
					dropoffData = dropoffInput.data('item-info');
					console.log("pickup data after item-info");
					console.log(pickupData);

					console.log("dropoff data after item-info");
					console.log(pickupData);
					pickupInput.removeData('item-info');
					dropoffInput.removeData('item-info');
					sendGeoRequest(pickupData, function (res) {
						pickupData = res;
						console.log("pickup data");
						console.log(pickupData);
						sendGeoRequest(dropoffData, function (res) {

							dropoffData = res;
							console.log("dropoff data");
							console.log(dropoffData);
							$.ajax({
								type: 'POST',
								url: 'http://sandbox.e-zbookings.com/api/v1/carprice',
								dataType: 'json',
								processData: false,
								contentType: 'application/json',
								data: JSON.stringify({
									type: 'PP',
									passengers: 1,
								 	luggage: 1,
								 	date: datePickerInput.val(),
								 	time: timePickerInput.val(),
								 	pickup: pickupData,
								 	dropoff: dropoffData
								 }),

								/* real settings end */
								success: function (response) {
									var table = _.template($('#booking-car-price-tpl').html())({
											carPrices: response.prices
										}),
										tableRow,
										bookTableBtn;
									$($('#booking-car-price-tpl').attr('data-add-to')).html(table).show();
									body.removeClass(loaderClass);

									if(response.options !=  null) {
										for(i=0; i<response.options.length; i++) {
											var price = parseInt(response.options[i].price);
	 										var parking = parseInt(response.options[i].parking);
	 										var meetgreetprice = price + parking;
											var ifBaggage = null;
											if(response.options[i].name == "Baggage Claim") {
												$('#meetgreet').append("<option selected='selected'>"+response.options[i].name+" - $"+meetgreetprice+"</option>");
												ifBaggage = response.options[i].name + " - $" + meetgreetprice;
											}
											else if(response.options[i].name == "International") {
												var optVal = "some value";
												if (ifBaggage.length === 0 && typeof ifBaggage === "undefined") {
												  	$('#meetgreet').append("<option selected='selected'>"+response.options[i].name+" - $"+meetgreetprice+"</option>");
												}
												else {
														$('#meetgreet').append("<option>"+response.options[i].name+" - $"+meetgreetprice+"</option>");
												}
											}
											else {
												$('#meetgreet').append("<option>"+response.options[i].name+" - $"+meetgreetprice+"</option>");
											}
										}
									}
									else {
										$('#meetgreet').hide();
										$('#choosemg').hide();
									}

									table = $('#booking-car-price-holder');
									tableRow = table.find('tbody tr');
									bookTableBtn = table.find('[data-role="book"]');

									table.find('tbody tr:not(.btn-line, .select-line)').on({
										'mouseenter': function () {
											if (!table.hasClass(checkedClass)) {
												var cur = $(this),
													curRadio = cur.find('[type="radio"]');

												if(curRadio.length){
													curRadio[0].checked = true;
												}

												cur.addClass(activeClass);
											}
										},
										'mouseleave': function () {
											if (!table.hasClass(checkedClass)) {
												var cur = $(this),
													curRadio = cur.find('[type="radio"]');

												if(curRadio.length){
													curRadio[0].checked = false;
												}

												cur.removeClass(activeClass);
											}
										},
										'click': function () {
											var cur = $(this),
												curRadio = cur.find('[type="radio"]');

											if(curRadio.length){
												curRadio[0].checked = true;
											}
											table.addClass(checkedClass);
											tableRow.removeClass(activeClass);
											cur.addClass(activeClass);
											bookTableBtn.removeAttr('disabled');
										}
									});
									$("#book").click(function() {
											 var token = response.token;
											 var cur = $(this);
 										   curRadio = $('input[type="radio"]:checked').val();
											 if(response.options !=  null) {
											 var conceptName = $('#meetgreet').find(":selected").text();
											 var firstword = conceptName.charAt(0);
											 var link = 'http://sandbox.e-zbookings.com/ReviewJob?action=book&book={"token":'+token+',"car":'+curRadio+',"option":'+firstword+'}';
											 window.location.href = link;
										   }
											 else {
												 var link = 'http://sandbox.e-zbookings.com/ReviewJob?action=book&book={"token":'+token+',"car":'+curRadio+'}';
												 window.location.href = link;
											 }

											 $('#choosemg').hide();
											 $('#meetgreet').find('option').remove();
    								});
								},
								error: function (error) {
									console.log('error', error);
								}
							});
						});
					});
				} else {
					if (!pickupInput.data('item-info')) {
						pickupInput.val('');
					}

					if (!dropoffInput.data('item-info')) {
						dropoffInput.val('');
					}
				}
			}
		});

		pickupInput.add(dropoffInput).autocomplete({
			source: function (request, response) {
				var resultCollection = $.ui.autocomplete.filter(airportsSource, request.term).splice(0, options.maxItems);
				console.log(resultCollection);
				if (autocompleteService) {
					autocompleteService.getPlacePredictions({
						input: request.term
					}, function (places, status) {
						if (status == google.maps.places.PlacesServiceStatus.OK) {
							resultCollection = resultCollection.concat($.map(places || [], function (item) {
								return {
									code: item.place_id,
									label: item.description,
									isairport: false,
									googleData: item
								};
							}).splice(0, options.maxItems));
						}

						response(resultCollection);
					});
				} else {
					response(resultCollection);
				}
			},
			select: function (event, ui) {
				$(event.target).data('item-info', ui.item);
			},
			appendTo: '.menu-container'
		}).data('ui-autocomplete')._renderItem = function (ul, item) {
			return $('<li>')
				.data('item-info', item)
				.append(item.label)
				.appendTo(ul);
		};
	});
})(window, jQuery, window.airportsSource);
